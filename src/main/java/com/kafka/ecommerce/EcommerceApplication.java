package com.kafka.ecommerce;

import java.util.concurrent.ExecutionException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceApplication {

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    SpringApplication.run(EcommerceApplication.class, args);
  }

}

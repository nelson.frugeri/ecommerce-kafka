package com.kafka.ecommerce.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class OrderDto implements Serializable {

  private static final long serialVersionUID = 5640694683201075203L;

  private Long userId;
  private UUID orderId;
  private BigDecimal value;
}

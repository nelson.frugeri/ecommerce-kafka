package com.kafka.ecommerce.consumer;

import com.kafka.ecommerce.dto.OrderDto;
import com.kafka.ecommerce.serializer.GsonDeserializer;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.UUID;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

public class StoreConsumer {

  public static void main(String[] args) {
    var consumer = new KafkaConsumer<String, OrderDto>(properties());
    consumer.subscribe(Collections.singletonList("store_new_order"));

    while (true) {
      var records = consumer.poll(Duration.ofMillis(100));

      if (!records.isEmpty()) {
        System.out.println("------------------------");
        System.out.println("Processing new order...");

        records.forEach(record -> {
          System.out.println(record.key());
          System.out.println(record.value());
          System.out.println((record).partition());
          System.out.println(record.offset());
        });
      }
    }
  }

  private static Properties properties() {
    var properties = new Properties();
    properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");

    properties.setProperty(
        ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
        StringDeserializer.class.getName());
    properties.setProperty(
        ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
        GsonDeserializer.class.getName());
    properties.setProperty(
        ConsumerConfig.GROUP_ID_CONFIG,
        StoreConsumer.class.getSimpleName());
    properties.setProperty(
        ConsumerConfig.CLIENT_ID_CONFIG,
        StoreConsumer.class.getSimpleName() + UUID.randomUUID().toString());
    properties.setProperty(
        ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "1");
    properties.setProperty(
        GsonDeserializer.TYPE_CONFIG, OrderDto.class.getName());

    return properties;
  }
}

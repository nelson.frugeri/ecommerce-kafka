package com.kafka.ecommerce.producer;

import com.kafka.ecommerce.dto.OrderDto;
import com.kafka.ecommerce.serializer.GsonSerializer;
import java.math.BigDecimal;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class StoreProducer {

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    for (int i = 0; i < 10; i++) {
      var order = OrderDto.builder()
          .userId((long) i)
          .orderId(UUID.randomUUID())
          .value(BigDecimal.valueOf(Math.random() * 5000 * 1))
          .build();

      final var producer = new KafkaProducer<String, OrderDto>(properties());

      var record = new ProducerRecord<>(
          "store_new_order", UUID.randomUUID().toString(), order);

      producer.send(record, (data, exception) -> {
        if (exception != null) {
          exception.printStackTrace();
          return;
        }
        System.out.println(data.topic() + ":::" + data.partition() + "/" +
            data.offset() + "/" + data.timestamp() + order);
      }).get();
    }
  }

  private static Properties properties() {
    var properties = new Properties();

    properties.setProperty(
        ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
        "127.0.0.1:9092");
    properties.setProperty(
        ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
        StringSerializer.class.getName());
    properties.setProperty(
        ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
        GsonSerializer.class.getName());
    properties.setProperty(
        ProducerConfig.ACKS_CONFIG, "all");

    return properties;
  }
}
